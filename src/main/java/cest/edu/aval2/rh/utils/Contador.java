package cest.edu.aval2.rh.utils;

public class Contador {

    static public Object getInstance(Contador contador){
       return contador.getClass();
    }
    static public Integer getProxNumero(Integer numero){
        return numero + 1;
    }

    public Contador() {
    }
}
