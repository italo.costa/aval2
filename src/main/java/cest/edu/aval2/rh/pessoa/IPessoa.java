package cest.edu.aval2.rh.pessoa;

public interface IPessoa {
    public String getNome();
    public String getEndereco();
}
