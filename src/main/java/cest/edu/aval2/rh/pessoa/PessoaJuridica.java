package cest.edu.aval2.rh.pessoa;

import cest.edu.aval2.rh.endereco.Endereco;

public class PessoaJuridica implements IPessoa {
    private String nome;
    private String cnpj;
    private Endereco endereco;

    @Override
    public String getNome() {
        return this.nome;
    }

    @Override
    public String getEndereco() {
        return this.endereco.toString();
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public PessoaJuridica(String nome, String cnpj) {
        this.nome = nome;
        this.cnpj = cnpj;
    }
}
