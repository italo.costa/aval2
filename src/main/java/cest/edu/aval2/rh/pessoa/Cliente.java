package cest.edu.aval2.rh.pessoa;

import cest.edu.aval2.rh.atendimento.Atendimento;

import java.util.List;

public class Cliente extends PessoaFisica{
    private final List<Atendimento> atendimentos;

    public List<Atendimento> getAtendimentos() {
        return atendimentos;
    }

    public Cliente(String nome, String cpf, List<Atendimento> atendimentos) {
        super(nome, cpf);
        this.atendimentos = atendimentos;
    }
}
