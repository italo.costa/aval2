package cest.edu.aval2.rh.endereco;

public class Cidade {
    private UF uf;
    private String nome;

    public UF getUf() {
        return uf;
    }

    public void setUf(UF uf) {
        this.uf = uf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cidade(UF uf, String nome) {
        this.uf = uf;
        this.nome = nome;
    }
}
