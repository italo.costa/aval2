package cest.edu.aval2.rh.atendimento;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Atendimento {
    private Date data;
    private Integer numero;

    private List<Requisicoes> requisicoes;

    public void abrirRequisicao(String nome){

        this.requisicoes.add(new Requisicoes(nome));
    }

    public void encerraAtendimento(){
        for (Requisicoes requisicao : this.requisicoes) {
            requisicao = null;
        }
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Atendimento(Date data, Integer numero) {
        this.data = data;
        this.numero = numero;
        this.requisicoes = new ArrayList<>();
    }


    @Override
    public String toString() {
        return "Atendimento{" +
                "data=" + data +
                ", numero=" + numero +
                ", requisicoes=" + requisicoes.stream().map(req -> String.valueOf(req.getNome())).collect(Collectors.toList()) +
                '}';
    }
}
