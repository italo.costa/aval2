package cest.edu.aval2.rh.atendimento;

class Requisicoes {
    String nome;

    public Requisicoes(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
