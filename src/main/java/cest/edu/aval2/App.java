package cest.edu.aval2;

import cest.edu.aval2.rh.atendimento.Atendimento;
import cest.edu.aval2.rh.utils.Contador;

import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        Contador contador = new Contador();
        System.out.println(Contador.getProxNumero(1));
        System.out.println(Contador.getInstance(contador));

        Atendimento atendimento = new Atendimento(new Date(), 777);
        atendimento.abrirRequisicao("teste");
        atendimento.abrirRequisicao("teste2");
        atendimento.abrirRequisicao("teste3");

        System.out.println(atendimento);
    }


}
